
import { Box, Heading } from 'grommet'
import React from "react"
import Hero from "../components/Hero"
import Intro from "../components/Intro"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Workshops from "../components/Workshops"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <Hero></Hero>
    
    <Intro></Intro> 
      
    <Box background="white" align="center" direction="column" pad={{vertical: 'medium'}} >
      
      <Box direction="column" pad={{ horizontal: 'medium' }}  width={{ max: 'xlarge' }} fill>
        <Heading level={2} color="pink">Workshops</Heading>
        <Box direction="row-responsive"  >
          <Workshops></Workshops>
        </Box>
      </Box>
    </Box>

  </Layout>
)

export default IndexPage
