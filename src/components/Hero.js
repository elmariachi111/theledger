import { Box, Heading } from 'grommet'
import React, { useEffect, useState } from 'react'
import styled from 'styled-components'

const MyHeading = styled(Heading)`
  white-space: nowrap;
  font-weight: 200;
  max-width: inherit;
  span {
    transition: color 2000ms ease;
  }
`


function AlienFont({text, speed=1000}) {

  const [cur, setCur] = useState(0) 
  useEffect( () => {
    const intvl = setInterval( () => { setCur( (cur+1) % text.length) }, speed);
    return () => clearInterval(intvl);
  }) 

  return <>{text.split('').map( (l,i) => { 
    const style = i === cur ? {color: "#898ee5"} : {}
    return <span style={style}>{l}</span>
  })}
  </>
}

export default (props) => (
    <>
    <Box background="darkblue" 
        direction="column" 
        align="center" justify="end"
        >
          <MyHeading level={2} 
              size="26vw"  margin="none" 
              color="lightblue"
              alignText="center"
            >
            <AlienFont text="the ledger"></AlienFont>
          </MyHeading>

       
    </Box>

    <Box background="lightblue"  
         
         direction="column" 
          justify="start"
          align="center"
         >
      {/* border={{ color: "white", size: "small", side: "top"}}*/ }
      
      <MyHeading level={2} size="18vw" color="darkblue" margin="none" >
        <AlienFont text="workshop day" speed={750}></AlienFont>
      </MyHeading>
          
    </Box>
    </>
) 