import { Anchor, Box } from 'grommet'
import React from 'react'
import { DosisText } from './DosisText'
import { Header } from './grommet-master/Header'

export default function TheHeader ({ props }) {
  /*const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  const thesize = React.useContext(ResponsiveContext)
*/
  return <Header background="verydarkblue" pad="small" id="theheader">

    <Box direction="row" align="center" justify="around" fill>
      <Box>
        <Anchor href="/" size="large">
          the ledger
        </Anchor>
      </Box>

      <Box >
        <DosisText color="green" size="large">Sat, 2020 / 4 / 4</DosisText>
      </Box>

      <Box >
          <DosisText color="pink" size="xlarge" alignSelf="right">only online</DosisText>
      </Box>

    </Box>
  </Header>
}
