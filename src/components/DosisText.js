import { Text } from 'grommet'
import styled from 'styled-components'

export const DosisText = styled(Text)`
  font-family: Dosis, sans-serif;
`