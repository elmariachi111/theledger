import { Box, Image,Heading, Paragraph, Text } from 'grommet'
import React from 'react'
import isoledger from '../images/illu.png'

export default () => (
    <Box background="darkblue" align="center" pad={{vertical: 'medium'}} direction="row-responsive" justify="center" 
         style={{position:"relative"}}>
    
        <Box basis="2/3" pad={{ horizontal: 'medium' }} width={{ max: 'large' }} style={{ zIndex: 5}}>
            <Heading level={2} color="pink">
            learn to ledger
            </Heading>

            <Paragraph fill>
            "Do we need a blockchain for that?" - and if so, which one is best suited?
            And how to develop meaningful applications for it?
            
            Everybody does the blockchain talk today. The tech is about
            to change any kind of operation as we know it as it proceeds to 
            mature in many aspects.
            </Paragraph>
            <Paragraph fill >
            If you ever wondered, how all that stuff works: <Text color="green">We've got your back</Text>. 
            </Paragraph>
            <Paragraph fill>
            Since you're likely working from home these days anyway, we're inviting
            you to a <b>full day online event</b> to hack along workshops provided
            by blockchain evangelists. 
            </Paragraph>
            <Paragraph fill>
            This one is for developers and technicians - folks who code meaningful
            software and need some help to get started.
            </Paragraph>
        </Box>

        <Box  basis="1/4">
            <Image src={isoledger} width="400px" alt="the ledger" 
            style={{position:"absolute", top:"-8vh", right:"5vw", zIndex: "1"}}
            />
        </Box>
    </Box>
)