import React from 'react'
import {Box, Text, Paragraph, Anchor, Image} from 'grommet'

import lisk from '../images/logos/lisk.svg'
import tbox from '../images/logos/3box.svg'
import parity from '../images/logos/parity.svg'
import near from '../images/logos/near.svg'
import { DosisText } from './DosisText'

function Workshop({logo, name, link="", children}) {
    return (
    <Box basis="1/2"  pad="small"  direction="column" justify="start" >
        <Box height={{min: "200px"}} justify="center" align="center">
            <Anchor href={link} target="_blank">
            <Image src={logo} alt={`${name} logo`} width="100%" />
            </Anchor>
        </Box>
        <Box>
            <DosisText size="large" color="pink" textAlign="center" >{name}</DosisText>
            {children}
        </Box>
        
    </Box>
    )
}

export default function Workshops(props) {
return (
    <>
    <Workshop logo={tbox} name="3box">
        <Paragraph fill>
        3Box makes it simple to store data securely with your users, letting you focus on building great products. #BuildBetter        
        </Paragraph>        
    </Workshop>
    <Workshop logo={parity} name="Parity" link="//www.parity.io/">
        <Paragraph fill>
        From Parity Ethereum, the most advanced Ethereum client, to Polkadot, the next-generation interoperable blockchain network. Parity builds the cutting edge of Web 3.0.        
        </Paragraph>        
    </Workshop>
    <Workshop logo={near} name="NEAR">
        <Paragraph fill>
        Write, test and deploy scalable decentralized applications in minutes on the most developer-friendly blockchain.        
        </Paragraph>        
    </Workshop>
    <Workshop logo={lisk} name="Lisk">
        <Paragraph fill>
        Lisk is a blockchain application platform. We make blockchain accessible by creating a world in which everyone benefits from this technology.        
        </Paragraph>        
    </Workshop>
    </>
)

} 