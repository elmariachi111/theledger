import { Anchor, Box, Grommet, Text } from 'grommet'
import React from 'react'
import '../css/typography.css'
import { Footer, Main } from './grommet-master'
import Header from './Header'
import theme from './theme'



export default function Layout ({ children }) {
  return (
    <Grommet theme={theme} themeMode="dark" >
      <Header />

      <Main >
        {children}

      </Main>

      <Footer
        background="verydarkblue"
        pad={{ horizontal: 'large', vertical: 'small' }}
      >
        <Box direction="row" gap="small">
          <Text alignSelf="center" color="pale">the ledger</Text>
        </Box>
        <Text textAlign="center" size="small">
          © 2020 | <Anchor href="//coding.earth">coding.earth</Anchor> | <Anchor href="//coding.earth/imprint">imprint</Anchor>
        </Text>
        
      </Footer>
    </Grommet >
  )
}
