import { dark } from 'grommet/themes'
import { deepMerge } from 'grommet/utils'

const theme = deepMerge(dark, {
  global: {
    colors: {
      pale: '#fff1d7',
      dark: '#222222',
      verydarkblue: '#181d82',
      darkblue: '#262dbf',
      lightblue: '#2e36d1',
      verylightblue: '#898ee5',
      pink: '#dd1397',
      green: '#97dd13'
    },
    
    body: {
      background: 'dark-1'
    },

    font: {
      family: 'Roboto',
      size: '16px',
      height: '20px'
    }
  },
  anchor: {
    color: {
      dark: 'pink',
      light: 'pink'
    },
  },
  heading: {
    font: {
      family: 'Dosis, sans-serif',
      weight: 200
    }
  }
})

/*

font-family: 'Play', sans-serif;
font-family: 'Orbitron', sans-serif;
font-family: 'Poiret One', cursive;
font-family: 'Staatliches', cursive;
font-family: 'Julius Sans One', sans-serif;
*/

export default theme
